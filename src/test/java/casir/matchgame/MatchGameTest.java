package casir.matchgame;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.Exception;

class MatchGameTest {

    @BeforeEach // this function is called before each test
    void setUp(){
        // init context
    }
    
    @Test 
    void calculateRemainingGiveOne() {
        int c = MatchGame.calculateRemaining(2, 1);
        assertEquals(1, c);
    }
    
    @Test 
    void shouldReturnTrue() {
        boolean c = MatchGame.hasWin(1);
        assertEquals(true, c);
    }
    
    @Test 
    void shouldReturnFalse() {
    	boolean c = MatchGame.hasWin(2);
        assertEquals(false, c);
    }
    
    @Test 
    void shouldReturnTrue2() {
    	boolean c = MatchGame.hasLose(1);
        assertEquals(true, c);
    }
    
    @Test 
    void shouldReturnFalse2() {
    	boolean c = MatchGame.hasLose(2);
        assertEquals(false, c);
    }
    
    @Test 
    void shouldDisplayYouWin() {
    	MatchGame.displayResult("You win !");
    }
    
    @Test 
    void shouldDisplayRemainingToFive() {
    	MatchGame.displayRemaining(5);
    }

}
