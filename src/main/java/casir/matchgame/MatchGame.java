package casir.matchgame;

import java.lang.Exception;
import java.util.Scanner;
import java.util.Random;

public class MatchGame {

    public static void main(String[] args){
        int remaining = 10;
        while (true) {
            Scanner reader = new Scanner(System.in);
            
            // Saisie
            int n = letUserEnterANumber(reader);
            remaining = calculateRemaining(remaining, n);
            
            // Vérification si gagné
            if (hasWin(remaining)){
                displayResult("You Win");
                break;
            }
            
            // Reste
            displayRemaining(remaining);
            
            // Tour de l'ordi
            n = new Random().nextInt(2) + 1;
            remaining = calculateRemaining(remaining, n);
            
            // Vérification si perdu
            if (hasLose(remaining)) {
            	displayResult("You Loose");
                break;
            }
            
            displayRemaining(remaining);
        }
    }

    public static int calculateRemaining(int remaining, int numberToSubtract) {
    	return remaining - numberToSubtract;
    }
    
    public static int letUserEnterANumber(Scanner reader) {
    	System.out.println("Enter a number between 1 and 3: ");
        int n = reader.nextInt();
        
        return n;
    }
    
    public static void displayRemaining(int remaining) {
    	System.out.println("Remaining " + remaining);
    }
    
    public static void displayResult(String result) {
    	System.out.println(result);
    }
    
    public static boolean hasWin(int remaining) {
    	return remaining == 1;
    }
    
    public static boolean hasLose(int remaining) {
    	return remaining == 1;
    }
    
}